// console.log('Hello World');

let trainer = {};


    trainer.name = "Ash Ketchum";
    trainer.age = 10;
    trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
    trainer.friends = {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    };

    trainer.talk = function() {
        console.log('Pikachu! I choose you!');
    };

    console.log(trainer);

    console.log('Results from dot notation: ');
    console.log(trainer.name);

    console.log('Results from square bracket notation: ');
    console.log(trainer['pokemon']);

    console.log('Results from talk method: ');
    trainer.talk();

    function Pokemon(name, level) {
        this.name = name;
        this.level = level;
        this.health = level * 2;
        this.attack = level * 1.5;
        this.tackle = function(target){
            console.log(this.name + ' tackled ' + target.name);

            target.health -= this.attack;
            console.log(target.name + ' health is now reduced to ' + target.health);

            if (target.health <= 0) {
                target.faint();
            }

            console.log(target);
        }
        this.faint = function(){
            console.log(this.name + ' fainted.');
        }
    }

    let pikachu = new Pokemon('Pikachu', 12,);
    console.log(pikachu);

    let geodude = new Pokemon('Geodude', 8);
    console.log(geodude);

    let mewtwo = new Pokemon('Mewtwo', 100);
    console.log(mewtwo);